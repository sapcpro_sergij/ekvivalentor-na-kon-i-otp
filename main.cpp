#include <iostream>
using namespace std;

class Element{
private:
    double vrednost;
    double vrednost2;
public:
    double paralelnaVrska_kon(double a, double b){
        vrednost = a;
        vrednost2 = b;
        return vrednost + vrednost2;
    }
    double seriskaVrska_kon(double a, double b){
        vrednost = a;
        vrednost2 = b;
        return (vrednost * vrednost2) / (vrednost + vrednost2);
    }
    double paralelnaVrska_otp(double a, double b){
        vrednost = a;
        vrednost2 = b;
        return (vrednost * vrednost2) / (vrednost + vrednost2);
    }
    double seriskaVrska_otp(double a, double b){
        vrednost = a;
        vrednost2 = b;
        return vrednost + vrednost2;
    }
};

int main(){
    string element, vrska;
    double a, b, suma = 0;

    cout<<"Odberi element (kondenzator/otpornik): ";
    cin>>element;
    if(element == "kondenzator" || element == "k"){
        Element kondenzator;
        cout<<"Vnesete ja vrednosta na prviot kondenzator: ";
        cin>>a;
        cout<<"Vnesete ja vrednost na vtoriot kondenzator: ";
        cin>>b;
        cout<<"Vnesete ja vrskata (paralelna/seriska): ";
        cin>>vrska;
        if(vrska == "paralelna" || vrska == "p"){
            suma = kondenzator.paralelnaVrska_kon(a, b);
        }
        else if(vrska == "seriska" || vrska == "s"){
            suma = kondenzator.seriskaVrska_kon(a, b);
        }
        else {
            cout<<"Izbravte nevalidna vrska! Validni opcii se: paralelna/p i seriska/s.";
        }
        cout<<"Rezultatot e: "<<suma;
    }
    else if(element == "otpornik" || element == "o"){
        Element otpornik;
        cout<<"Vnesete ja vrednosta na prviot otpornik: ";
        cin>>a;
        cout<<"Vnesete ja vrednost na vtoriot otpornik: ";
        cin>>b;
        cout<<"Vnesete ja vrskata (paralelna/seriska): ";
        cin>>vrska;
        if(vrska == "paralelna" || vrska == "p"){
            suma = otpornik.paralelnaVrska_otp(a, b);
        }
        else if(vrska == "seriska" || vrska == "s"){
            suma = otpornik.seriskaVrska_otp(a, b);
        }
        else {
            cout<<"Izbravte nevalidna vrska! Validni opcii se: paralelna/p i seriska/s.";
        }
        cout<<"Rezultatot e: "<<suma;
    }
    else {
        cout<<"Izbravte nevaliden element! Validni opcii se: kondenzator/k i otpornik/o.";
    }
    return 0;
}
